package uz.pdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppAutoCalonApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppAutoCalonApplication.class, args);
    }

}

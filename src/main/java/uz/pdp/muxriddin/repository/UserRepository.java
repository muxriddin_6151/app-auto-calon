package uz.pdp.muxriddin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.muxriddin.entity.User;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
}

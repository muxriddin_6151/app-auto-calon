package uz.pdp.muxriddin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.muxriddin.entity.Mashina;

import java.util.UUID;

public interface MashinaRepository extends JpaRepository<Mashina, UUID> {
}

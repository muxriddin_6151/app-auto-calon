package uz.pdp.muxriddin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.muxriddin.entity.Cars;

import java.util.UUID;

public interface CarsRepository extends JpaRepository<Cars, UUID> {
}
